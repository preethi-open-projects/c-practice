#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>
#include"word_analysis.h"

uint16_t count_punctuation(char *paragraph)
{
	uint16_t index=0,punctuation_count=0;

	while(paragraph[index++]!='\0')
	{
		if(paragraph[index]=='.' || paragraph[index]==',' || paragraph[index]=='?' || paragraph[index]==';' || paragraph[index]=='!')

			punctuation_count++;
	}

	return punctuation_count;
}

uint16_t count_word(char *paragraph)
{
	uint16_t index=0,word_count=0;

	while(paragraph[index++]!='\0')
	{
		if(paragraph[index]==' ' && paragraph[index+1]!=' ')
			word_count++;

	}

	return word_count;
}

uint16_t count_sentence(char *paragraph)
{
	uint16_t index=0,sentence_count=0;
while(paragraph[index++]!='\0')
	{
		//loop to count sentences
		if(paragraph[index]=='.' || paragraph[index]=='?' || paragraph[index]=='!')
			sentence_count++;
	}
	
	return sentence_count;
}

