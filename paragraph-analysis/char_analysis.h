#ifndef _CHAR_H_
#define _CHAR_H_

#include<stdint.h>


uint16_t count_unique_char(char *paragraph);
uint16_t count_letters(char *paragraph);
uint16_t count_vowels(char *paragraph);
uint16_t count_white_spaces(char *paragraph);

#endif
