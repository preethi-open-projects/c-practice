#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>
#include"char_analysis.h"

uint16_t count_unique_char(char *paragraph)
{
	uint16_t index=0,unique_count=0,flag=false;

	while(paragraph[index++]!='\0')
	{
		 for(int j=0;j<index;j++)
                {
                        if(paragraph[j]==paragraph[index])
                                flag=true;
			else if(paragraph[index]=='.' || paragraph[index]==' ' || paragraph[index]==',')
				flag=true;
                }

                if(flag!=true)
                {
                       unique_count++;
                }

                flag=false;
	}

	return unique_count;
}

uint16_t count_letters(char *paragraph)
{
	uint16_t index=0,letters_count=0;

	while(paragraph[index++]!='\0')
	{
		if(paragraph[index]!=' ' && paragraph[index]!='.' && paragraph[index]!=',' && paragraph[index]!='?' && paragraph[index]!=';' && paragraph[index]!='!')
			
			letters_count++;
	}

	return letters_count;
}

uint16_t count_vowels(char *paragraph)
{
	uint16_t index=0,vowels_count=0;

	while(paragraph[index++]!='\0')
	{
		if(paragraph[index]=='a' || paragraph[index]=='e' || paragraph[index]=='i' || paragraph[index]=='o' || paragraph[index]=='u' || paragraph[index]=='A' || paragraph[index]=='E' || paragraph[index]=='I' || paragraph[index]=='O' || paragraph[index]=='U')
			
				vowels_count++;
	}

	return vowels_count;
}


uint16_t count_white_spaces(char *paragraph)
{
	uint16_t index=0,white_spaces_count=0;

	while(paragraph[index++]!='\0')
	{
		if(paragraph[index]==' ')
			white_spaces_count++;
	}

	return white_spaces_count;
}
