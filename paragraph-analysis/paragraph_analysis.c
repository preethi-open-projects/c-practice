#include<stdio.h>
#include<stdint.h>
#include "char_analysis.h"
#include "word_analysis.h"

int main(void)
{
	char paragraph[]={"Hello World."};

	uint16_t unique_char=0,letters=0,vowels=0,white_spaces=0;
	uint16_t punctuation=0,words=0,sentence=0;

	punctuation=count_punctuation(paragraph);
	printf("The punctuation count is %d\n",punctuation);

	words=count_word(paragraph);
	printf("The words count is %d\n",words+1);

	sentence=count_sentence(paragraph);
	printf("The sentence count is %d\n",sentence);

	unique_char=count_unique_char(paragraph);
	printf("The unique characters are %d\n",unique_char);

	letters=count_letters(paragraph);
	printf("The letters are %d\n",letters);

	vowels=count_vowels(paragraph);
	printf("The vowels are %d\n",vowels);

	white_spaces=count_white_spaces(paragraph);
	printf("The white spaces are %d\n",white_spaces);

	return 0;
}
