#ifndef _WORD_H_
#define _WORD_H_

#include<stdint.h>


uint16_t count_punctuation(char *paragraph);
uint16_t count_word(char *paragraph);
uint16_t count_sentence(char *paragraph);

#endif
