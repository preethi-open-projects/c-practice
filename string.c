#include<stdio.h>
#include<string.h>

int main(void)
{	
	char first_string[] = {"Hi"} , second_string[] = {"Namaste"}, third_string[] = {"goodday"},fourth_string[50],fifth_string[50],res;
	
	//string copy function 
	//test case input : {"Hi"}
	strcpy(fourth_string,first_string);
	printf("The copied string using strcpy is %s\n",fourth_string);
	

	//string concatenation function
	//test case input : first_string = {"Hi"} and second_string = {"Namaste"}
	strcat(first_string,second_string);
	printf("The concatenated string using strcat is %s\n",first_string);
	

	//string comparison function
	//test case input: second_string = {"Namaste"} and first_string = {"Hi"}
	res = strcmp(second_string,first_string);
	if (res == 0) 
        	printf("Strings are equal\n"); 
    	else
        	printf("Strings are unequal\n"); 
  	

	//function to find string length
	//test case input : first_string = {"HiNamaste"}
	printf("The length of the string is %d\n",strlen(first_string));


	//function to concatenate one string with part of another
	//test case input : first_string = {"HiNamaste"} and third_string = {"goodday"}
	strncat(first_string,third_string,4);
	printf("The concatenated string using strncat is %s\n",first_string);

	
	//function to compare part of string to another
	//test case input : first_string = {"HiNamaste"} and second_string = {"Namaste"}
	int result = strncmp(first_string , second_string, 4); 
  
   	 if (result == 0) 
	 { 
         	printf("str1 is equal to str2 upto num characters\n"); 
    	 } 
    	else if (result > 0) 
        	printf("str1 is greater than str2\n"); 
    	else
        	printf("str2 is greater than str1\n"); 



	//function to copy part of the string to another string
	strncpy(fifth_string,first_string,4);
	printf("The copied string after strncpy is %s\n",fifth_string);

	
	//function to find characters after last occurence character
	
	char ch = 'e',str[100]={"Hello"};
	int val = strrchr(str,ch);
	printf("String after last character %c is %s\n",ch,val);
	return 0;
}
