#ifndef _ADD_H_
#define _ADD_H_

#include<stdint.h>

uint16_t add_numbers(uint16_t num1, uint16_t num2);

#endif
