#include<stdio.h>
#include<stdint.h>
#include "add.h"

int main(void)
{
	uint16_t a=10,b=5,sum=0;

	sum = add_numbers(a,b);
	printf("%hhu+%hhu=%hhu\n",a,b,sum);

	return 0;
}
