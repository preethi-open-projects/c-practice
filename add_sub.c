#include<stdio.h>
#include<stdint.h>

uint16_t number_function(uint16_t *num1, uint16_t num2, uint16_t *subtracted_value);

int main()
{
	uint16_t a=30,b=10;
	uint16_t difference=0;
	uint16_t sum=0;

	sum = number_function(&a,b,&difference);

	printf("The sum is : %u+%u=%u\n",a,b,sum);
	printf("The difference is : %u-%u=%u\n",a,b,difference);

	return 0;
}

uint16_t number_function(uint16_t *num1, uint16_t num2, uint16_t *subtracted_value)
{
	if(num1==NULL || subtracted_value==NULL)
	{
		return 0;
	}

	 
	uint16_t added_value;

	added_value = *num1+num2;
	
	*subtracted_value = *num1-num2;

	return added_value;
}

//test cases for the program : a=5, b=20 and a=30 and b=10.
