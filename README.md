# c-practice

 C programs for practice.Repository to create and practice C programs.

 # List of files

1.add_num : adds two numbers using .h and .c files.

2.multiply_num : multiplies two numbers using .c and .h files.

3.paragraph-analysis : analyses the paragraph using .c and .h files(to count vowels,sentence etc..).

4.add_pointer_arg : adds three numbers using pointer arguments.

5.add_sub : adds and subtracts two values using pointer arguments.

6.alnum : checks whether a character is alphanumeric or not.

7.array-copy : copies elements from one array to another.

8.arraysum : adds elements of an array.

9.asciitable : prints the ASCII table.

10.bin_to_int : converts a binary value into an integer.

11.build : builds all c files.

12.case-conversion : converts lower case alphabets to upper case alphabets and vice versa.

13.check : checks whether a character is a lower case alphabet,upper case alphabet ,a special character or a number.

14.copy_n_bytes : copies n bytes in an array.

15.enum : prints enum text and its values.

16.fibonacci : prints fibonacci series upto a given range.

17.hex_to_int : converts hexadecimal value to an integer.

18.hex_to_string : converts hexadecimal value to a string.

19.mult_build : builds multi-file projects which include c and header files.

20.multiply : multiplies a number with 10 without * operator.

21.pointer-arg : adds two numbers using pointer argument.

22.pointer-arithmetic : performs pointer arithmetic using different data types.

23.pointer_stringlen : calculates string length using pointer arguments.

24.print :  checks if the characters is printable or not.

25.recursion : prints number from 1 to N.

26.return_num : returns the corresponding number from a string.

27.rtruncate : reverse truncates a string upto a given truncate length.

28.strconcat : concatenates two strings.

29.strcpy : copies one string to another using pointer argument.

30.swap : swaps two numbers without using third variable.

31.truncate : truncates a string upto a given truncate length.

32.unique : counts the number of unique characters in a string.

 # How to run each program
 ```sh
 $ sudo apt-get install build-essential(one-time usage)
 $./build.sh <c_file_name> && ./<c_filename.bin>
 ```

