#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>

uint64_t hex_to_int(char *hex_value, uint16_t length);

int main(void)
{
	char hex_value[] ={"fff"};
	uint64_t conversion=0;
	uint64_t length =  sizeof(hex_value);
	
	conversion=hex_to_int(hex_value,length);

	printf("The converted decimal value of %s is %lu\n",hex_value,conversion);
	

	return 0;
}

uint64_t hex_to_int(char *hex_value , uint16_t length)
{
	if(hex_value==NULL)
		return 0;

	uint64_t base=1;
	uint64_t dec_val=0;
	int index;
	
	//loops to check invalid input
	if(hex_value[0]>='g' && hex_value[0]<='z')
	{
		printf("Invalid input\n");
		exit(0);
	}

	for(index=2;index<=length;index++)
	{
		if(hex_value[index]>='g' && hex_value[index]<='z')
		{
			printf("Invalid input\n");
			exit(0);
		}
	}

	//loop for conversion
	for(index=length;index>=0;index--)
	{
		if(hex_value[index]>='0' && hex_value[index]<='9')
		{
			dec_val+=(hex_value[index]-'0')*base;

			base*=16;
		}

		else if(hex_value[index]>='a' && hex_value[index]<='f')
		{
			dec_val += (hex_value[index]-'W')*base;

			base*=16;
		}

		else if(hex_value[index]>='A' && hex_value[index]<='F')
		{
			dec_val += (hex_value[index]-'7')*base;

			base*=16;
		}

	}

	return dec_val;

}

//test cases for the program : hex_value= "ff" , "0xff" , "FFFFFFFF", "AB" ,"cd" "fghffu"
