#!/bin/bash

file_name=$1
total_args=$#
script_name=$0
binary_name=${file_name::-1}bin
gcc_args="-Wall -Wextra -Werror"

########################################### Function
func_usage() {
	echo
	echo "Use: $script_name <c_filename>"
	echo

}

func_check_arg_validity() {
	if ! [ $total_args -eq 1 ] ; then
		echo "Invalid number of arguments"
		func_usage
		exit 1
	fi

	if ! [ -e $file_name ] ; then
		echo "$file_name doesnot exists."
		exit 1
	fi
}


############################################ Script starts here

func_check_arg_validity

gcc $gcc_args $file_name -o $binary_name && echo "Generated binary file: $binary_name"  


