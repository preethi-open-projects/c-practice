#include<stdio.h>

int main(void)
{
        char str[50] = {"FooBarBaz"};
        int n , strlen=0 , i=0;

        printf("Enter the truncate length");
        scanf("%d",&n);

        while(str[i++]!='\0')
                strlen++;


        str[strlen - n] = '\0';
        printf("\nThe truncated value is %s\n",str);

        return 0;
}


//test cases : str = FooBarBaz,truncate length = 5  and str = HelloWorld , truncate length = 4
