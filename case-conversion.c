#include<stdio.h>
#include<string.h>
#include<stdint.h>

void to_lower(char *string,uint8_t length);


int main(void)
{
	char string[]={"HelloWorld"};
	uint8_t length = strlen(string);
		
	to_lower(string,length);

	return 0;
}


void to_lower(char *string,uint8_t length)
{
	if(string==NULL)
		return;


	uint8_t index=0;

	for(index=0;index<length;index++)
	{
		if(string[index]>='A' && string[index]<='Z')
		{
			string[index]+=32;
		}
	}

	printf("The converted string is %s\n",string);
}

//test cases for the program string = "ABC" , "AbCdE" and "HelloWorld"

