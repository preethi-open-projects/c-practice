#include<stdio.h>
#include<stdint.h>

void strcopy(char *str2, char *str1);

int main(void)
{
	char string1[]={"FooBarBaz"};
	char string2[100];
	
	printf("The original string in string1 is %s\n",string1);

	strcopy(string2,string1);
	return 0;
}


void strcopy(char *str2, char *str1)
{
	if(str2==NULL || str1==NULL)
		return;

	uint8_t index=0;

	for(index=0;str1[index]!='\0';index++)
	{
		str2[index]=str1[index];
	}

	printf("The copied string in string2 is %s\n",str2);
}

//test cases for the program string1 = "HelloWorld", "FooBarBaz" , "GoodMorning"
