#ifndef _MATH_OPERATION
#define _MATH_OPERATION

enum operation_type
{
	MATH_ADD,
	MATH_SUBTRACT,
	MATH_MULTIPLY 
};

uint16_t math_operation(enum operation_type , uint8_t , uint8_t);


#endif
