#include<stdio.h>
#include<stdint.h>
#include "math_operations.h"

static uint16_t add(uint8_t num1, uint8_t num2)
{
	return (num1+num2);
}

static uint16_t subtract(uint8_t num1 , uint8_t num2)
{
        return (num1-num2);
}

static uint16_t multiply(uint8_t num1 , uint8_t num2)
{
        return(num1*num2);
}

uint16_t math_operation(enum operation_type op , uint8_t num1 , uint8_t num2)
{
	uint16_t result=0;	
	switch(op)
	{
		case MATH_ADD:
			result = add(num1,num2);
			break;

		case MATH_SUBTRACT:
			result = subtract(num1,num2);
			break;

		case MATH_MULTIPLY:
			result = multiply(num1,num2);
			break;

		default:
			printf("Invalid\n");
	}
	return result;
}
