#include<stdio.h>
#include<stdint.h>
#include "math_operations.h"

int main(void)
{
	uint8_t a=5;
	uint8_t b=3;
	uint16_t result;
	
	enum operation_type op = MATH_MULTIPLY;	
	result=math_operation(op,a,b);
	
	
	if(op==MATH_ADD)
		printf("Sum=%d\n",result);

	else if(op==MATH_SUBTRACT)
		printf("Difference=%d\n",result);
	
	else
		printf("Product=%d\n",result);
	
}
