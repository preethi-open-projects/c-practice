#!/bin/bash

gcc_args="-Wall -Wextra -Werror"

gcc $gcc_args -c -o math_operations.o math_operations.c &&\
gcc $gcc_args -c -o main.o main.c &&\
gcc -o test.bin math_operations.o main.o &&\
rm *.o

