#include<stdio.h>
#include<stdint.h>

void arr_copy(uint16_t *src, uint16_t *dest,uint16_t length);

int main(void)
{
	uint16_t src[]={0,1,2,3,4,5};
	uint16_t dest[100];

	uint16_t length=sizeof(src)/sizeof(src[0]);
	
	arr_copy(src,dest,length);
	
	return 0;
}

void arr_copy(uint16_t *src , uint16_t *dest, uint16_t length)
{
	if(src==NULL || dest==NULL)
		return;

	uint16_t index=0;

	for(index=0;index<length;index++)
	{
		dest[index]=src[index];
	}

	printf("The copied array in destination is :");
	for(index=0;index<length;index++)
	{
		printf(" %d",dest[index]);
	}

	printf("\n");
}


//test cases for the program : arr1= {0,1,2,3,4,5,6,7,8,9} and {0,1,2,3,4,5}

