#include<stdio.h>
#include<stdint.h>

uint8_t add_num(uint8_t *num1, uint8_t num2);

int main(void)
{
	uint8_t a=10,b=5;

	uint8_t sum = add_num(&a,b);

	printf("%u+%u=%u\n",a,b,sum);

	return 0;
}

uint8_t add_num(uint8_t *num1, uint8_t num2)
{
	if(num1==NULL)
	{
		return 0;
	}

	uint8_t sum;

	sum = *num1+num2;
	
	return sum;
}
