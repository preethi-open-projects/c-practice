#include<stdio.h>
#include<stdlib.h>

void hex_to_string(int *hex_val,int *conversion);

int main(void)
{
	int hex_val=0xABCDe;
	int conversion[32];
	int *hex = &hex_val;
	
	hex_to_string(hex,conversion);
	
	return 0;
}

void hex_to_string(int *hex_val, int *conversion)
{
	if(hex_val==NULL && conversion==NULL)
	{
		return ;
	}

	int len=sizeof(*hex_val);

	int j=0,index;
	
	//c0nversion of hexadecimal to string
	for(int index=len;index>=0;index--)
		{
			conversion[index]+=((*hex_val>>j)&0xf);
			j=j+4;	
		}

	printf("The converted string is 0x");
	for(index=0;index<=len;index++)
	{
		printf("%x",conversion[index]);
	}

	printf("\n");

}
