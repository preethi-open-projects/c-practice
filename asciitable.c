#include<stdio.h>

void ascii(int n1, int n2)
{
        int value;

        printf(" Decimal | Hexadecimal | char\n");

	for(value = n1; value <= n2; value++)
 	       printf(" %d          %X         %c\n",value,value,value);
			
}


int main(void)
{
        int n1,n2;


        printf("Enter the lower limit :");
        scanf("%d",&n1);

	printf("Enter the upper limit :");
	scanf("%d",&n2);

        ascii(n1,n2);

        return 0;
}
                                                 

//test cases : range 0 to 50 and ranige 0 to 126
