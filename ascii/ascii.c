#include<stdio.h>
#include<stdint.h>
#include "ascii.h"


uint8_t ascii_to_lower(uint8_t ch)
{
	if(ch>='A' && ch<='Z')
		return(ch+32);
	else
		return ch;
}

uint8_t ascii_to_upper(uint8_t ch)
{
	if(ch>='a' && ch<='z')
		return(ch-32);
	else
		return ch;
}

void ascii_debug_info(uint8_t ch,info_function func)
{
	func(ch);
}
