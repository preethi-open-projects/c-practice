#include<stdio.h>
#include<stdint.h>
#include "ascii.h"


static void hex_value(uint8_t ch);
static void to_words(uint8_t ch);

int main(void)
{
	ascii_debug_info('b',hex_value);
	ascii_debug_info('9',to_words);
	return 0;
}

static void hex_value(uint8_t ch)
{
	printf("Hex value (%c) : 0x%02X\r\n",ch,ch);
}

static void to_words(uint8_t ch)
{
	if(ch=='0')
		printf("%c:ZERO\r\n",ch);
	else if(ch=='1')
		printf("%c:ONE\r\n",ch);
	else if(ch=='2')
		printf("%c:TWO\r\n",ch);
	else if(ch=='3')
		printf("%c:THREE\r\n",ch);
	else if(ch=='4')
		printf("%c:FOUR\r\n",ch);
	else if(ch=='5')
		printf("%c:FIVE\r\n",ch);
	else if(ch=='6')
		printf("%c:SIX\r\n",ch);
	else if(ch=='7')
		printf("%c:SEVEN\r\n",ch);
	else if(ch=='8')
		printf("%c:EIGHT\r\n",ch);
	else if(ch=='9')
		printf("%c:NINE\r\n",ch);
}
