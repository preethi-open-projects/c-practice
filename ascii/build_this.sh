#!/bin/bash

gcc_args="-Wall -Wextra -Werror"

gcc $gcc_args -c -o ascii.o ascii.c &&\
gcc $gcc_args -c -o main.o main.c &&\
gcc -o test.bin ascii.o main.o &&\
rm *.o

