#include<stdio.h>
#include<stdint.h>
#include "mul.h"

int main(void)
{
	uint16_t a=10,b=8,product=0;

	product=mul_num(a,b);
	printf("%hhu*%hhu=%hhu\n",a,b,product);

	return 0;
}
