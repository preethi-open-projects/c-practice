#!/bin/bash

gcc_args="-Wall -Wextra -Werror"

gcc $gcc_args -c -o mul.o mul.c &&\
gcc $gcc_args -c -o main.o main.c &&\
gcc -o test.bin mul.o main.o &&\
rm *.o

