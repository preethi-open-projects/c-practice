#include<stdio.h>
#include<stdbool.h>

int main()
{
        char str[]={"AAAAAA"};
        bool flag=false;
        int count=1;

        for(int i=1;str[i]!='\0';i++)
        {
                for(int j=0;j<i;j++)
                {
                        if(str[j]==str[i])
                                flag=true;
                }

                if(flag!=true)
                {
                        count++;
                }

                flag=false;
        }

        printf("The number of unique characters is %d\n",count);



        return 0;
}


//After debugging the program by printing the values of the for loops.The indexing was wrong.
//Instead of forward comparison reverse comparison was the solution.
//The program works for all the test cases provided.
