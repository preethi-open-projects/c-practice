#include<stdio.h>
#include<stdint.h>

uint8_t add_num(uint8_t *num1, uint8_t *num2, uint8_t *num3);

int main(void)
{
        uint8_t a=10,b=5,c=15;

        uint8_t sum = add_num(&a,&b,&c);

        printf("%u+%u+%u=%u\n",a,b,c,sum);

        return 0;
}

uint8_t add_num(uint8_t *num1, uint8_t *num2, uint8_t *num3)
{
        if(num1==NULL || num2==NULL || num3==NULL)
        {
                return 0;
        }

        uint8_t sum;

        sum = *num1+*num2+*num3;

        return sum;
}

