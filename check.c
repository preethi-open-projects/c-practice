#include<stdio.h>
#include<stdlib.h>

int main()
{
	char num; 
	printf("Enter the char : \n");
	scanf("%c", &num);

	if (num >= 'a' && num <= 'z')
	{
		printf(" %c is a lower case alphabet\n", num);
	}

	else if (num >= 'A' && num <= 'Z')
		printf(" %c is an upper case alphabet\n", num);

	else if (num >= '0' && num <= '9')
		printf("%c is a number\n", num);
	
	else 
		printf("%c is a special character\n", num);
}
