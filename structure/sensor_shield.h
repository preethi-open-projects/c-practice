#ifndef _SENSOR_H
#define _SENSOR_H

#include<stdint.h>

struct sensor_data{
	uint8_t temperature;
	uint8_t moisture;
};

uint8_t temperature_check(struct sensor_data );
uint8_t moisture_check(struct sensor_data);
uint8_t average_temperature(struct sensor_data *sample,uint8_t number_of_samples);
uint8_t total_average(struct sensor_data *sample,uint8_t number_of_samples);

#endif
