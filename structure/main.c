#include<stdio.h>
#include<stdint.h>
#include "sensor_shield.h"

#define NUMBER_OF_SAMPLES 2

int main(void)
{
	uint16_t temperature_result;
	uint16_t moisture_result;
	struct sensor_data s1_data;
	struct sensor_data sample[NUMBER_OF_SAMPLES];

	s1_data.temperature=50;
	s1_data.moisture=45;

	temperature_result = temperature_check(s1_data);
	if(temperature_result==1)
		printf("Temperature sensor: The sensor sample is in safe range\n");
	else
		printf("Temperature sensor: False\n");

	moisture_result = moisture_check(s1_data);
	if(moisture_result==1)
		printf("Moisture sensor: The soil is dry\n");
	else
		printf("Moisture sensor: The soil is not dry\n");

	sample[0].temperature=40;
	sample[1].temperature=20;

	printf("Average temperature:%d\r\n",average_temperature(sample,NUMBER_OF_SAMPLES));
	
	sample[0].moisture=120;
	sample[1].moisture=100;

	printf("Total Average :%d\r\n",total_average(sample,NUMBER_OF_SAMPLES));


	return 0;
}
