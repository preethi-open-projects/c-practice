#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>
#include "sensor_shield.h"

uint8_t temperature_check(struct sensor_data result)
{

	if(result.temperature>=15 && result.temperature<=35)
		return true;
	else 
		return false;
}

uint8_t moisture_check(struct sensor_data result)
{
	if(result.moisture<100)
		return true;
	else 
		return false;
}

uint8_t average_temperature(struct sensor_data *sample,uint8_t number_of_samples)
{
	uint16_t result=0;
	uint16_t i;

	for(i=0;i<number_of_samples;i++)
		result+=sample[i].temperature;

	return (result/number_of_samples);
}

uint8_t total_average(struct sensor_data *sample,uint8_t number_of_samples)
{
	uint16_t total=0,number_of_sensors=2,i;
	uint16_t temperature=0,moisture=0;
	uint16_t temperature_avg=0;
	uint16_t moisture_avg=0;

	for(i=0;i<number_of_samples;i++)
		temperature+=sample[i].temperature;

	for(i=0;i<number_of_samples;i++)
		moisture+=sample[i].moisture;

	temperature_avg=temperature/number_of_samples;
	moisture_avg=moisture/number_of_samples;
	total=temperature_avg+moisture_avg;

	return (total/number_of_sensors);

}
