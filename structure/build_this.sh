#!/bin/bash

gcc_args="-Wall -Wextra -Werror"

gcc $gcc_args -c -o sensor_shield.o sensor_shield.c &&\
gcc $gcc_args -c -o main.o main.c &&\
gcc -o test.bin sensor_shield.o main.o &&\
rm *.o

