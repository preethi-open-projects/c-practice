#include<stdio.h>
#include<stdint.h>

uint16_t string_length(char *string);

int main(void)
{
	char string[] = {"HelloWorld"};

	uint16_t length=string_length(string);

	printf("The length of the string is %hhu\n",length);

	return 0;
}

uint16_t string_length(char *string)
{
	if(string==NULL)
	{
		return 0;
	}

	uint16_t index=0,count=0;

	while(string[index++]!='\0')
	{
		count++;		
	}

	return count;
}

//test cases for the program string ="HelloWorld" and "Preethi"
