#include<stdio.h>
#include<stdlib.h>

int string_int(char *string );

int main(void)
{

        char str[]={"-789"};
        int result;

        result = string_int(str);

        printf("Equivalent integer value is %i\n",result);

        return 0;
}

int string_int(char *string)
{
        if(string == NULL)
                return 0;

        int index=0,sum=0,sign=1;

	if (string[0] == '-') 
	{
        	sign = -1;
        	index++;
    	}

        for(;string[index]!='\0';index++)
	{
		if(string[index]<'0' || string[index]>'9')
		{
			printf("Invalid number\n");
			exit(0);
		}

		else
        	{	
          	sum = sum*10 + (string[index]-'0');
        	}
	}
 

        return sign*sum;
}

//test cases for thr program string= "1234" "-123" "A" , "567" and "-789"
