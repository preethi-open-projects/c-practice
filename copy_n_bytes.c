#include<stdio.h>
#include<string.h>
#include<stdint.h>

void print_array(uint8_t *array , uint16_t length);
void memory_copy(void *target, void *source , size_t n);

int main(void)
{
	uint8_t arr1[]={10,20,30,40,50,60,70,80,90};
	int n=5;

	printf("Before copying\n");
	printf("arr1:"); 
	print_array(arr1,sizeof(arr1)/sizeof(arr1[0]));
	
	memory_copy(arr1,arr1+2,n);
	printf("After copying\n");
	print_array(arr1,sizeof(arr1)/sizeof(arr1[0]));

	return 0;
}

void print_array(uint8_t *array,uint16_t length)
{
	int index;
	for(index=0;index<length;index++)
		printf("%d ",array[index]);
	printf("\n");
}

void memory_copy(void *target, void *source, size_t n)
{
	uint16_t index;

	char *t = (char *)target;
	char *s = (char *)source;

	if(s<t)
		for(index=n;index>0;index--)
		{
			t[index]=s[index];
		}
	

	else if(s>t)
	{
		for(index=0;index<=n;index++)
			t[index]=s[index];
	}


}

