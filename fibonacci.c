#include<stdio.h>
#include<stdint.h>

int fibonacci( int n)
{
	if(n == 0)
		return 0;
	
	else if(n == 1)
		return 1;
	
	else 
		return (fibonacci(n-1) + fibonacci(n-2));

}

int main(void)
{
	int n , i=0 , j;

	printf("Enter the limit ");
	scanf("%d",&n);
	
	printf("The fibonacci series is :\n");

	for(j=1;j<=n;j++)
	{
		printf("%d\n",fibonacci(i));
		i++;
	}
	return 0;

}

//input tested for the program are n =5 and n =10.
