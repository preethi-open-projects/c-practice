#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>

bool valid_binary_char(char ch);
bool binary_string_to_int(char *binary_string, uint32_t *number);
uint8_t get_binary_number_from_char(char ch);

int main(void)
{

        char binary_string[]={"11111"};
       	uint32_t converted_num;
	bool result;
	
	result=binary_string_to_int(binary_string,&converted_num);

	if(result==false)
		return -1;

	printf("Decimal value of the number %s is %d {0x%X}\n",binary_string,converted_num,converted_num);

        return 0;
}

bool binary_string_to_int(char *binary_string , uint32_t *number)
{
  	if(!binary_string || !number)
		return 0;

	uint32_t converted_int;
	uint32_t binary_string_len;
	uint32_t index;

	for(index=0;binary_string[index]!='\0';index++)
	{

	}
	binary_string_len=index;

	if(binary_string_len==0)
	{
		printf("Empty string provided.\n");
		return false;
	}

	if(binary_string_len>=2)
	{
		if(binary_string[0]=='0'&& binary_string[1]=='b')
			index=2;
		else
			index=0;
	}

	converted_int=0;
	for(;binary_string[index]!='\0';index++)
	{
		if(valid_binary_char(binary_string[index])==false)
		{
			printf("Error: %c is not a valid character in string %s.\n",binary_string[index],binary_string);
			printf("Valid binary characters are [0&1]\n");
			return false;
		}

		converted_int=converted_int<<1;
		converted_int=converted_int|(get_binary_number_from_char(binary_string[index]));
	}

	*number=converted_int;
	return true;
}

bool valid_binary_char(char ch)
{
	if(ch=='0' || ch=='1')
		return true;
	else 
		return false;
}

uint8_t get_binary_number_from_char(char ch)
{
	if(ch>='0' && ch<='1')
		return (ch-'0');
	else 
		return false;
}



//test cases for program : bin_value= 0111,0b111,1011,0b11111111,and 11g11
